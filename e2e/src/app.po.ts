import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  navigateToSubPage() {
    return browser.get(browser.baseUrl + 'todo/y') as Promise<any>;
  }

  getTitleText() {
    return element(by.css('app-root h1.title')).getText() as Promise<string>;
  }

  addNewToDo() {
    element(by.css('app-root input')).sendKeys('Test ToDo');
    element.all(by.css('app-root .button.is-success')).first().click();
  }

  async getLastToDoText() {
    const testToDo = element.all(by.css('app-root .todos li .title-small')).getText()
      .then((texts) => {
        return texts[texts.length - 1];
      });

    return await testToDo;
  }

  goToDetailPageAndEditTitle() {
    element.all(by.css('app-root .button.is-warning')).last().click();
    element(by.css('app-root input')).sendKeys('Test Edited ToDo');
    element(by.css('app-root .button.is-success')).click();

    return element(by.css('app-root .todo-detail-title')).getText() as Promise<string>;
  }

  markToDoAsDone() {
    try {
      element(by.css('app-root .button.is-warning')).click();
      return element(by.css('app-root .button.is-warning .unfinished')).getText();
    } catch (error) {
      console.error(error);
      return false;
    }
  }

  deleteToDo() {
    try {
      element.all(by.css('app-root .button.is-danger')).click();
    } catch (error) {
      console.error(error);
      return false;
    }

    return true;
  }
}

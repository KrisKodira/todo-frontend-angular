import { AppPage } from './app.po';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display todo title', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('My ToDo-List');
  });

  it('should save a new todo', () => {
    page.navigateTo();
    page.addNewToDo();
    expect(page.getLastToDoText()).toEqual('Test ToDo');
  });

  it('should go to detail page and edit ToDo title', () => {
    expect(page.goToDetailPageAndEditTitle()).toEqual('Test Edited ToDo');
  });

  it('should mark ToDo as done', () => {
    expect(page.markToDoAsDone()).toEqual('unfinished');
  });

  it('should delete ToDo', () => {
    expect(page.deleteToDo()).toBeTruthy();
  });

  it('should display todo not found', () => {
    page.navigateToSubPage();
    expect(page.getTitleText()).toEqual('Sadly the ToDo you\'re looking for can\'t be found.');
  });
});

import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ApiService } from '../api.service';
import { ToDo } from '../todo';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.sass']
})
export class TodoListComponent implements OnInit {

  todos: ToDo[] = [];
  headers: string[] = [];

  postdata: ToDo;

  allToDos: any;

  showingFinishedToDos = false;

  @ViewChild('toDoTitle', {static: false}) toDoTitle: ElementRef;

  constructor(private api: ApiService) {}

  getToDos() {
    this.api.getToDos()
      .subscribe(data => {
        this.allToDos = data.filter((toDo: ToDo) => {
          return toDo.isDone === false;
        });
        this.showingFinishedToDos = false;
      });
  }

  getDoneToDos() {
    this.api.getToDos()
      .subscribe(data => {
        this.allToDos = data.filter((toDo: ToDo) => {
          return toDo.isDone === true;
        });
        this.showingFinishedToDos = true;
      });
  }

  getToDoById(id: string) {
    this.api.getToDoById(id)
      .subscribe(data => {
        return data;
      });
  }

  addToDo() {
    this.api
      .addToDo(this.postdata)
      .subscribe(resp => {
        console.log(resp);
      });
  }

  updateToDo(id: string) {
    this.api
      .updateToDo(id, this.postdata)
      .subscribe(resp => {
        this.getToDos();
      });
  }

  deleteToDo(id: any) {
    this.api
      .deleteToDo(id)
      .subscribe(resp => {
        this.getDoneToDos();
      });
  }

  saveNewToDo() {
    const toDoTitle = this.toDoTitle.nativeElement.value.toString();
    if (toDoTitle.trim() !== '') {
      this.postdata = new ToDo(toDoTitle, false);
      this.api.addToDo(this.postdata).subscribe(data => {
        this.toDoTitle.nativeElement.value = '';
        this.getToDos();
      });
    }
  }

  finishToDo(toDoId) {
    this.api.getToDoById(toDoId)
      .subscribe(data => {
        const updatedToDo = new ToDo(data.title, true, data.id);
        this.postdata = updatedToDo;
        this.updateToDo(toDoId);
      });
  }

  ngOnInit() {
    this.getToDos();
  }

}

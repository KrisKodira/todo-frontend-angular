import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../api.service';
import { ToDo } from '../todo';

@Component({
  selector: 'app-todo-detail',
  templateUrl: './todo-detail.component.html',
  styleUrls: ['./todo-detail.component.sass']
})

export class TodoDetailComponent implements OnInit {

  todo: ToDo;
  paramId: string;

  @ViewChild('toDoTitleDetail', {static: false}) toDoTitleDetail: ElementRef;

  constructor(private apiService: ApiService, private route: ActivatedRoute, private router: Router) { }

  updateToDoStatus() {
    const updatedToDo = new ToDo(this.todo.title, !this.todo.isDone, this.todo.id);
    this.apiService.updateToDo(this.todo.id, updatedToDo).subscribe(() => {
      this.getCurrentToDo();
    });
  }

  updateToDoTitle() {
    const toDoTitle = this.toDoTitleDetail.nativeElement.value.toString();
    if (toDoTitle.trim() !== '') {
      const updatedToDo = new ToDo(toDoTitle, this.todo.isDone, this.todo.id);
      this.apiService.updateToDo(this.todo.id, updatedToDo).subscribe(() => {
        this.getCurrentToDo();
        this.toDoTitleDetail.nativeElement.value = '';
      });
    }
  }

  getCurrentToDo() {
    this.apiService.getToDoById(this.paramId).subscribe(t => {
      this.todo = t;
    });
  }

  deleteCurrentToDo() {
    this.apiService.deleteToDo(this.todo.id).subscribe(() => {
      this.router.navigate(['/']);
    });
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.paramId = params.get('id');
      this.getCurrentToDo();
    });
  }

}

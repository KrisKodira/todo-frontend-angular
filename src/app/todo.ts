export class ToDo {

  constructor(title?: string, isDone?: boolean, id?: string) {

    this.title = title;
    this.isDone = isDone;

    if (title !== undefined) {
      this.title = title;
    }

    if (isDone !== undefined) {
      this.isDone = isDone;
    }

    if (id !== undefined) {
      this.id = id;
    }
  }

  public id?: string;
  public title: string;
  public isDone: boolean;

  getTitle(): string {
    return this.title;
  }

  getId(): string {
    return this.id;
  }

  getIsDone(): boolean {
    return this.isDone;
  }
}

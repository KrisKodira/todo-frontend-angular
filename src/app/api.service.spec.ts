import { TestBed, getTestBed, async } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';


import { ApiService } from './api.service';
import { ToDo } from './todo';
import { of } from 'rxjs';

describe('ApiService', () => {

  let injector: TestBed;
  let service: ApiService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        ApiService
      ]
    });
    injector = getTestBed();
    service = injector.get(ApiService);
    httpMock = injector.get(HttpTestingController);
  });


  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    const service: ApiService = TestBed.get(ApiService);
    expect(service).toBeTruthy();
  });

  it('should get all ToDos', async(() => {
    const testToDoToGet: ToDo[] = [new ToDo('Test1', true, '1234567890'), new ToDo('Test2', false, '0987654321')];

    spyOn(service, 'getToDos').and.callThrough().and.returnValue(of(testToDoToGet));
    service.getToDos().subscribe((resp: ToDo[]) => {
      console.log('sub resp 1 ', resp);
      expect(service.getToDos).toHaveBeenCalled();
      expect(resp[0].title).toEqual('Test1');
    });
  }));

  it('should get ToDo by id', async(() => {
    const testToDoToGet: ToDo = new ToDo('Test2', true, '1234567890');

    spyOn(service, 'getToDoById').and.callThrough().and.returnValue(of(testToDoToGet));
    service.getToDoById('1234567890').subscribe((resp: ToDo) => {
      console.log('sub resp 2 ', resp);
      expect(service.getToDoById).toHaveBeenCalled();
      expect(resp.id).toEqual('1234567890');
    });
  }));

  it('should call create ToDo function', async(() => {
    const toDoToAdd = new ToDo('Test3', true, '0987654321');

    spyOn(service, 'addToDo').and.callThrough().and.returnValue(of(toDoToAdd));
    service.addToDo(toDoToAdd).subscribe((resp) => {
      console.log('sub resp 3 ', resp);
      expect(service.addToDo).toHaveBeenCalled();
    });
  }));

  it('should call delete ToDo function', async(() => {
    const toDoToDelete = '1234567890';

    spyOn(service, 'deleteToDo').and.callThrough().and.returnValue(of(toDoToDelete));
    service.deleteToDo(toDoToDelete).subscribe((resp) => {
      console.log('sub resp 4 ', resp);
      expect(service.deleteToDo).toHaveBeenCalled();
    });
  }));

  it('should call update ToDo function', async(() => {
    const toDoId = '0123456789';
    const toDoToUpdate = new ToDo('Test3', true, '0987654321');

    spyOn(service, 'updateToDo').and.callThrough().and.returnValue(of(toDoToUpdate));
    service.updateToDo(toDoId, toDoToUpdate).subscribe((resp) => {
      console.log('sub resp 5 ', resp);
      expect(service.updateToDo).toHaveBeenCalled();
    });
  }));


});

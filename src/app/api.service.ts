import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToDo } from './todo';
import { Observable, of } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';


const localUrl = 'http://localhost:8080/todos/';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})

export class ApiService {

  constructor(private http: HttpClient) {}

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(message);
  }



  getToDos(): Observable<ToDo[]> {
    return this.http.get<ToDo[]>(localUrl, httpOptions)
      .pipe(
        retry(3), catchError(this.handleError<ToDo[]>('getToDos', []))
      );
  }

  getToDoById(id: string): Observable<ToDo> {
    return this.http.get<ToDo>(localUrl + id, httpOptions)
      .pipe(
        retry(3), catchError(this.handleError<ToDo>('getToDoById'))
      );
  }

  addToDo(todo: ToDo): Observable<ToDo> {
    return this.http.post<ToDo>(localUrl, todo, httpOptions)
      .pipe(
        catchError(this.handleError('addToDo', todo))
      );
  }

  deleteToDo(id: string) {
    return this.http.delete(localUrl + id, httpOptions)
      .pipe(
        catchError(this.handleError('deleteToDo'))
      );
  }

  updateToDo(id: string, todo: ToDo): Observable<ToDo> {
    return this.http.put<ToDo>(localUrl + id, todo, httpOptions)
      .pipe(
        catchError(this.handleError('updateToDo', todo))
      );
  }

}
